package com.abc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class MostFrequentFlyer {

    public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1);
        private final Text word = new Text();

        /**
         * For the given key-value pair, this function extracts the first part of the value (delimited by commas) as the passenger ID and writes it to the context with a count of 1.
         *
         * @param key     The input key, which is not used in this function.
         * @param value   The input value expected to be a comma-separated string where the first field represents the passenger ID.
         * @param context The context used for outputting key-value pairs.
         */
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            // Split the input value by commas and extract the passenger ID
            String[] parts = value.toString().split(",");
            String passengerId = parts[0];
            // Set the word to the extracted passenger ID
            word.set(passengerId);
            // Write the passenger ID with a count of 1 into the context
            context.write(word, one);
        }

    }

    public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        private int maxFlights = 0;
        private final Text mostFrequentFlyer = new Text("None");

        /**
         * Processes key-value pairs in the reduce phase, summing up integer values associated with each key,
         * and identifying the key with the maximum sum.
         * If the sum associated with the current key is greater than the previously recorded maximum sum,
         * it updates the maximum sum and the corresponding key.
         *
         * @param key     The key representing a set of data entries sharing a common characteristic.
         * @param values  An iterable collection of integer values associated with the key.
         * @param context The context object used to output the processed results.
         */
        public void reduce(Text key, Iterable<IntWritable> values, Context context) {
            int sum = 0;
            // Iterates through all integer values associated with the current key and sums them up
            for (IntWritable val : values) {
                sum += val.get();
            }
            // If the sum for the current key exceeds the previously recorded maximum, updates the maximum and the key
            if (sum > maxFlights) {
                maxFlights = sum;
                mostFrequentFlyer.set(key);
            }
        }


        /**
         * Implements the cleanup logic for this MapReduce task.
         * This method is invoked at the end of either the Map or Reduce phase to perform any necessary cleanup work.
         * In this specific case, it writes out the most frequent flyer (mostFrequentFlyer) and their corresponding maximum number of flights (maxFlights)
         * into the Hadoop Distributed File System.
         *
         * @param context The Context object which provides access to read and write data within the MapReduce task.
         */
        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            // Writes out the most frequent flyer with their max number of flights to the context for further processing or storage.
            context.write(mostFrequentFlyer, new IntWritable(maxFlights));
        }

    }

    /**
     * The main entry point of the program, which executes a task to analyze the most frequent flyer.
     *
     * @param args Command-line arguments: args[0] is the input file path, and args[1] is the output file path.
     * @throws Exception If any error occurs during execution, an exception is thrown.
     */
    public static void main(String[] args) throws Exception {
        // Initialize Hadoop configuration and create a new job instance
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "most frequent flyer");

        // Set the job's driver class, combiner, and reducer
        job.setJarByClass(MostFrequentFlyer.class); // Specify the JAR file for the job
        job.setMapperClass(TokenizerMapper.class); // Set the mapper class
        job.setCombinerClass(IntSumReducer.class); // Set the combiner class
        job.setReducerClass(IntSumReducer.class); // Set the reducer class

        // Define the output key-value pair types
        job.setOutputKeyClass(Text.class); // Output key type
        job.setOutputValueClass(IntWritable.class); // Output value type

        // Set the input and output paths
        FileInputFormat.addInputPath(job, new Path(args[0])); // Set the input path
        FileOutputFormat.setOutputPath(job, new Path(args[1])); // Set the output path

        // Execute the job, wait for its completion, and then exit
        System.exit(job.waitForCompletion(true) ? 0 : 1); // Exit with code 0 if the job completes successfully, otherwise 1
    }

}
